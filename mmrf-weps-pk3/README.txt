======================================================================================
									Mega Man Rock Force
======================================================================================

The original fangame was a creation by GoldWaterDLS. Check out the official site!
http://megamanrockforce.com/rockforcemain.html
And the credits for the original fangame
http://megamanrockforce.com/rockforcecredits.html

Now updated to MM8BDM V6B by Russel and Trillster!
Load with the MMRF map pack for cool replacements!

X=========================X
| Original Weapon Credits |
X=========================X

All sound effects ripped by Gummywormz

Crypt Cloak - Maxine / Yellow Devil (+Jax)
Photon Flare - Maxine / Yellow Devil (+Jax)
Pulse Stopper - Maxine / Yellow Devil
Virus Outbreak - Trillster with on-hit effect by Maxine
Phantom Fuse - Maxine / Yellow Devil
Shock Gauntlet - Maxine / Yellow Devil with cleaner pulling by JaxOf7
Circuit Breaker - Maxine / Yellow Devil
Charade Clone - JaxOf7

X==============X
| Modder Notes |
X==============X

Translation IDs 3550-3558 used.
CustomBar Slot 42 used.
ClassBase Slot 42 used.
CBM ShootableActor Slot 42 used.