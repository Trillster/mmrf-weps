const int CONST_VAR_HP = 0;
const int CONST_VAR_LOANHP = 1;

actor ClassBaseSlot42 : ClassBaseSlot41 //RF Megaman
{
	var int user_hp;
	var int user_loanhp;
	States
	{
		Pain.CircuitBreaker:
			PLY1 H 0 
			Goto Pain+1
		Pain.VirusOutbreak:
			PLY1 H 0 A_GiveInventory("VirusProtection",1)
			PLY1 H 0 A_GiveInventory("VirusPain", 1)
			goto Pain+2
	}
}