actor PhotonFlareWep_NormalBar : NormalBar { args 4, 215 }
actor PhotonFlareWep_ScriptBar : ScriptBar {}

actor PhotonFlareWep : BaseMM8BDMWep
{
	//$Category MM8BDM-Weapons
	//$Title Photon Flare
	Weapon.AmmoUse 4
	Weapon.AmmoGive 28
	Weapon.SlotNumber 4
	Inventory.Pickupmessage "$PU_PHOTONFLARE"
	Obituary "$OB_PHOTONFLARE"
	Tag "$TAG_PHOTONFLARE"
	weapon.ammotype "PhotonFlareAmmo"
	inventory.icon "PHOFLI"
	States
	{
		SpawnLoop:
			PHFL A -1
			loop
			
		Ready:
			PHFL A 0 ACS_NamedExecuteWithResult("core_weaponcolor",CLR_PHOTONFLARE)
			PHFL E 0 A_JumpIfInventory("PhotonFlareCooldown",57,"ReadyNoFire")
			PHFL E 1 A_WeaponReady
			Goto Ready+1
		ReadyNoFire:
			PHFL E 1 A_WeaponReady(WRF_NOFIRE)
			Goto Ready+1
			
		Deselect:
			PHFL E 0
			goto DeselectSwap
		Select:
			PHFL E 0
			goto SelectSwap
			
		Fire:
			PHFL E 0 A_JumpIfNoAmmo("NoAmmo")
			PHFL E 0 A_SpawnItemEx("PhotonFlare",0,0,8,0,0,0,0)
			PHFL E 0 A_TakeInventory("PhotonFlareAmmo",4)
			PHFL E 0 A_PlaySoundEx("weapon/photon","Weapon")
			PHFL E 0 A_SpawnItemEx("PhotonWatcher")
			PHFL FGHI 2
			PHFL I 3
			PHFL IHGF 4
			PHFL E 2
			Goto Ready+1
			
		NoAmmo:
			PHFL E 1 ACS_NamedExecuteAlways("core_noammo",0)
			goto Ready+1
	}
}

actor PhotonFlareAmmo : Ammo
{
	inventory.amount 1
	inventory.maxamount 28
	+INVENTORY.IGNORESKILL
}

actor PhotonFlareCooldown : Inventory
{
	inventory.amount 1
	inventory.maxamount 112
}

actor PhotonFlare : BasicProjectile
{
	+SEEKERMISSILE
	+LOOKALLAROUND
	+DONTBLAST
	+BOUNCEONCEILINGS
	radius 16
	height 16
	speed 0
	damage (CallACS("mmrf_cbm_damage", 1))
	damagetype "PhotonFlare"
	Obituary "$OB_PHOTONFLARE"
	meleerange 256
	reactiontime 50
	bouncefactor 0.5
	States
	{
		Spawn:
			PHFL B 0
			PHFL BCDCBCDC 3 A_ChangeVelocity(0,0,3,CVF_REPLACE)
			PHFL BCDC 3 A_ChangeVelocity(0,0,2,CVF_REPLACE)
			PHFL BCDC 3 A_ChangeVelocity(0,0,1,CVF_REPLACE)
			PHFL B 0 A_Stop
		Look:
			PHFL B 1 A_SeekerMissile(4, 10, SMF_LOOK|SMF_PRECISE, 256, 2)
			PHFL BCDC 3 A_JumpIfTargetInLOS("Explode", 0, JLOSF_PROJECTILE, 256)
			PHFL B 0 A_CountDown
			loop
		Explode:
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,6,0,4,0)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,6,0,4,-45)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,6,0,4,45)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,6,0,4,90)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,6,0,4,-90)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,6,0,4,-135)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,6,0,4,135)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,6,0,4,180)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,12,0,4,0)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,12,0,4,-45)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,12,0,4,45)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,12,0,4,90)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,12,0,4,-90)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,12,0,4,-135)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,12,0,4,135)
			PHFL B 0 A_SpawnItemEx("PhotonFlare2",0,0,4,12,0,4,180)
		Death:
			NAPA A 0 A_Stop
			NAPA A 0 A_PlaySoundEx("misc/mm3explosion", "Weapon")
			NAPA D 0 A_Explode(CallACS("mmrf_cbm_damage", 24), 160, 0)
			NAPA FGHIJKLMNOPQRS 2
			stop
	}
}

actor PhotonFlare2 : BasicProjectile
{
	-NOGRAVITY
	Radius 6
	Height 7
	speed 0
	damage (CallACS("mmrf_cbm_damage", 1))
	damagetype "PhotonFlare"
	Obituary "$OB_PHOTONFLARE"
	reactiontime 15
	States
	{
		Spawn:
			PHFL A 0
			PHFL A 0 A_CountDown
			PHFL BC 3
			Goto Spawn+1
		Death:
			NAPA A 0 A_Stop
			NAPA A 0 A_PlaySoundEx("misc/mm3explosion", "Weapon")
			NAPA A 0 A_ChangeFlag("NOGRAVITY",1)
			NAPA D 0 A_Explode(CallACS("mmrf_cbm_damage", 24), 160, 0)
			NAPA FGHIJKLMNOPQRS 2
			stop
	}
}

actor PhotonWatcher : BasicWatcher
{
	States
	{
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_JumpIfInTargetInventory("PhotonFlareCooldown",1,"JustGive")
			TNT1 A 0 A_GiveToTarget("PhotonFlareCooldown",56)
			goto Spawn2
		JustGive:
			TNT1 A 0 A_GiveToTarget("PhotonFlareCooldown",56)
			stop
		Spawn2:
			TNT1 A 1
			TNT1 A 0 A_JumpIfInTargetInventory("PhotonFlareCooldown",1,1)
			stop
			TNT1 A 0 A_GiveInventory("CutterFlag",1)
			TNT1 A 0 A_JumpIfInventory("CutterFlag",12,"TakeCooldown")
			goto Spawn2
		TakeCooldown:
			TNT1 A 0 A_TakeFromTarget("PhotonFlareCooldown",1)
			TNT1 A 0 A_JumpIfInTargetInventory("PhotonFlareCooldown",1,1)
			stop
			TNT1 A 0 A_TakeInventory("CutterFlag",999)
			goto Spawn2
	}
}